export default class Clients extends Array {
  broadcast(data) {
    this.map(client => client.send(data));
  }

  clientConnect(client) {
    this.push(client);
  }

  clientDisconnect(client) {
    this.splice(
    	this.findIndex(el => {
      	return el == client
      }),
      1
    )
  }
}