import React from 'react';
import MessageFormat from './MessageFormat';
import './styles/chatColumn.css';

export default class ChatColumns extends React.Component {

  hasSender = () => {
    return (this.state.sender === '') ? 1 : 0;
  }

  catchSenderChange = evt => {
    this.setState({
      'sender': evt.target.value
    });
  }

  catchContentChange = evt => {
    this.setState({
      'content': evt.target.value
    });
  }

  handleSendEvent = evt => {
    evt.preventDefault();
    const sender = this.state.sender,
      content = this.state.content,
      testcase = [
        sender === '',
        content === '',
        sender.length > 20
      ];

    if(testcase.findIndex(el => {return el === true}) !== -1) return;

    this.props.wsSend(
      JSON.stringify(
        new MessageFormat(
          Date.now(),
          sender,
          content
        )
      )
    )
  }

  render() {
    return(
      <form id="wscommand" onSubmit={this.handleSendEvent}>
        <input
          type="text"
          className="sender"
          value={this.state.sender}
          onChange={this.catchSenderChange}
          required="required"
          placeholder="暱稱"
          pattern="([A-Z]|[a-z]|[0-9]){1,15}|[\u4e00-\u9eff]{1,6}"
          title="僅接受英文+數字15個字元或中文6個字元；請勿使用特殊符號" />
        <span>：</span>
        <input
          type="text"
          className="content"
          value={this.state.content}
          onChange={this.catchContentChange}
          required="required"
          maxLength="180"
          placeholder="訊息內容"
          pattern="^(\S)*"
          title="開頭不可空白" />
        <input
          type="submit"
          className="send"
          value="Send" />
      </form>
    )
  }

  constructor(props) {
    super(props);
    this.state = {
      'sender': '',
      'content': ''
    };
  }
}