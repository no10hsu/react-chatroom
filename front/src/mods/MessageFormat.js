export default class MessageFormat {
  constructor(date, sender, content) {
    this.date = date;
    this.sender = sender;
    this.content = content;
  }
}