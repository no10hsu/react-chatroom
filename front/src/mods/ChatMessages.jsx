import React from 'react';
import './styles/chatMessages.css';

export default class ChatMessages extends React.Component {
  showMessage = (msg) => {
    const displayTime = new Date(msg.date).toLocaleString();

    return(
      <div className="message" key={++this.key}>
        <div
          className="time"
          dangerouslySetInnerHTML={{__html: displayTime}} />
        <div
          className="sender"
          dangerouslySetInnerHTML={{__html: msg.sender}} />
        <span>:</span>
        <div
          className="context"
          dangerouslySetInnerHTML={{__html: msg.content}} />
      </div>
    );
  }

  componentDidUpdate() {
    let chatHistory = document.querySelector('#history')
    chatHistory.scrollTop = chatHistory.scrollHeight;
  }

  render() {
    return(
      <div id="history">
        {this.props.messages.map(this.showMessage)}
      </div>
    );
  }

  constructor(props) {
    super(props);
    this.key = -1;
  }
}