import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './mods/styles/index.css';

//import './index.css';
//import * as serviceWorker from './serviceWorker'; 官方的Service Worker

ReactDOM.render(<App />, document.body);


// 官方的Service Worker
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
//serviceWorker.unregister();
